import { createNodeDescriptor, INodeFunctionBaseParams } from "@cognigy/extension-tools";
import axios from 'axios';


export interface ICheckLiveAgentAvailabilityParams extends INodeFunctionBaseParams {
	config: {
		// connection: {
		// 	liveAgentUrl: string;
		// 	organizationId: string;
		// 	deploymentId: string;
		// 	livechatButtonId: string;
		// };
		liveAgentUrl: string;
		organizationId: string;
		deploymentId: string;
		livechatButtonId: string;
		storeLocation: string;
		contextKey: string;
		inputKey: string;
	};
}
export const checkLiveAgentAvailabilityNode = createNodeDescriptor({
	type: "checkLiveAgentAvailability",
	defaultLabel: "Check Live Agent Availability",
	fields: [
		// {
		// 	key: "connection",
		// 	label: "Livechat Credentials",
		// 	type: "connection",
		// 	params: {
		// 		connectionType: "salesforceLiveChat",
		// 		required: true
		// 	}
		// },
		{
			key: "liveAgentUrl",
			type: "cognigyText",
			label: "Live Agent Url",
			description: "The URL of your live agent"
		},
		{
			key: "livechatButtonId",
			type: "cognigyText",
			label: "Live Chat ButtonId",
			description: "The ID of your live chat button"
		},
		{
			key: "deploymentId",
			type: "cognigyText",
			label: "Deployment Id",
			description: "The ID of your Salesforce Deployment."
		},
		{
			key: "organizationId",
			type: "cognigyText",
			label: "Organization Id",
			description: "Your Salesforce Organization ID"
		},
		{
			key: "storeLocation",
			type: "select",
			label: "Where to store the result",
			defaultValue: "input",
			params: {
				options: [
					{
						label: "Input",
						value: "input"
					},
					{
						label: "Context",
						value: "context"
					}
				],
				required: true
			},
		},
		{
			key: "inputKey",
			type: "cognigyText",
			label: "Input Key to store Result",
			defaultValue: "liveAgentAvailability",
			condition: {
				key: "storeLocation",
				value: "input",
			}
		},
		{
			key: "contextKey",
			type: "cognigyText",
			label: "Context Key to store Result",
			defaultValue: "liveAgentAvailability",
			condition: {
				key: "storeLocation",
				value: "context",
			}
		},
	],
	sections: [
		{
			key: "storage",
			label: "Storage Option",
			defaultCollapsed: true,
			fields: [
				"storeLocation",
				"inputKey",
				"contextKey",
			]
		}
	],
	form: [
		// { type: "field", key: "connection" },
		{ type: "field", key: "liveAgentUrl" },
		{ type: "field", key: "organizationId" },
		{ type: "field", key: "deploymentId" },
		{ type: "field", key: "livechatButtonId" },
		{ type: "section", key: "storage" },
	],
	appearance: {
		color: "#009EDB"
	},
	function: async ({ cognigy, config }: ICheckLiveAgentAvailabilityParams) => {
		const { api, input } = cognigy;
		// const { connection, storeLocation, contextKey, inputKey } = config;
		const { storeLocation, contextKey, inputKey } = config;

		const { liveAgentUrl, organizationId, deploymentId, livechatButtonId } = config;
		if (!liveAgentUrl) throw new Error("The secret is missing the 'liveAgentUrl' key");
		if (!organizationId) throw new Error("The secret is missing the 'organizationId' key");
		if (!deploymentId) throw new Error("The secret is missing the 'deploymentId' key");
		if (!livechatButtonId) throw new Error("The secret is missing the 'livechatButtonId' key");

		try {
			const response = await axios({
				method: "GET",
				url: `${liveAgentUrl}/chat/rest/Visitor/Availability?org_id=${organizationId}&deployment_id=${deploymentId}&Availability.ids=${livechatButtonId}`,
				headers: {
					"X-LIVEAGENT-API-VERSION": '34',
				},
			});

			if (storeLocation === "context") {
				api.addToContext(contextKey, response.data, "simple");
			} else {
				// @ts-ignore
				api.addToInput(inputKey, response.data);
			}
		} catch (error) {
			if (storeLocation === "context") {
				api.addToContext(contextKey, error.message, "simple");
			} else {
				// @ts-ignore
				api.addToInput(inputKey, error.message);
			}
		}
	}
});